const a = document.getElementById("a")! as HTMLInputElement;
const b = document.getElementById("b")! as HTMLInputElement;
const addButton = document.querySelector("button")! as HTMLButtonElement;

function add(num1: number, num2: number) {
  return num1 + num2;
}

addButton.addEventListener("click", () => {
  console.log(add(+a.value, +b.value));
});

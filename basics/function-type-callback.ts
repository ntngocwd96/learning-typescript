function addAndHandle(n1: number, n2: number, cb: (num: number) => void) {
  const result = n1 + n2;
  cb(result);
}

addAndHandle(3, 22, (result) => {
  console.log(result);
});

/* ANOTHER WAY */
// function printResult(num: number) {
//   console.log(num);
// }

// addAndHandle(3, 33, printResult);

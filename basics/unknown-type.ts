let userName: unknown;
let userInput: string;
userName = 5;
userName = "Razos";

/* !!! ERROR !!!
  Type 'unknown' is not assignable to type 'string'.ts(2322)
  userInput = userName;
  But if we use 'any' we won't got error (Because any disable all type checking)
*/

if (typeof userName === "string") {
  userInput = userName;
}

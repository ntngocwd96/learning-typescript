function generateError(message: string, code: number): never {
  throw { message, errorCode: code };
}

generateError("An error occurred", 500);

/* !!! ERROR !!!
  Never type will crash script and doesnot return anything (including undefined)
console.log(generateError("An error occurred", 500));
*/

type Combine = number | string;
type ConversionDescription = "as-number" | "as-text";

function combine(
  input1: Combine,
  input2: Combine,
  resultConversion: ConversionDescription
) {
  let result: Combine;
  if (
    (typeof input1 === "number" && typeof input2 === "number") ||
    resultConversion === "as-number"
  ) {
    result = +input1 + +input2;
  } else {
    result = input1.toString() + input2.toString();
  }
  return result;
}

const combineAge = combine(15, 30, "as-number");
console.log(combineAge);

const combineStringAge = combine("15", "30", "as-number");
console.log(combineStringAge);

const combineName = combine("Crist", "Evan", "as-text");
console.log(combineName);

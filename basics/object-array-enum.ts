// const person: {
//   name: string;
//   age: number;
//   hobbies: string[];
//   role: [number, string]; // Tuple Types
// } = {

/* It will be fine */
// enum Role {
//   ADMIN,
//   READ_ONLY,
//   AUTHOR,
// }
enum Role {
  ADMIN = "ADMIN", // We can put any value (e.g ADMIN = 0, READ_ONLY = 100, AUTHOR = 'author')
  READ_ONLY = "READ ONLY",
  AUTHOR = "AUTHOR",
}
const person = {
  name: "Razos",
  age: 20,
  hobbies: ["Sport", "Game"],
  // role: [1, "author"], // Tuple Types Example
  role: Role.AUTHOR
};

// person.role = [1, "reader", "female"]; // If dont let Typescript infer we will got error
// person.role.push("male");

console.log(person);

for (const hobby of person.hobbies) {
  console.log(hobby.toLocaleUpperCase());
  // console.log(hobby.map(() => {})); // Property 'map' does not exist on type 'string' !!! ERROR !!!
}

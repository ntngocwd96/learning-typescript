function add(n1: number, n2: number) {
  return n1 + n2;
}

function printResult(num: number) {
  console.log("Result is: ", num);
}

printResult(add(3, 5));

// let combineValues: Function;
let combineValues: (a: number, b: number) => number;
combineValues = add;
/* !!! ERROR !!!
  Type '(num: number) => void' is not assignable to type '(a: number, b: number) => number'.
  Type 'void' is not assignable to type 'number'.ts(2322)
  combineValues = printResult;
*/

console.log(combineValues(12, 8));
